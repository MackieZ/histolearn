const initialState = {
  email: '',
  loading: false,
  defaultLesson: 0,
  roomCode: '',
  roomName: '',
  roomDescription: '',
  roomTime: 10,
  roomMode: 'choice',
  roomStatus: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'AUTH_SUCCESS':
      return {
        ...state,
        email: action.value
      }

    case 'DEFAULT_LESSON':
      return {
        ...state,
        defaultLesson: state.defaultLesson !== action.value && action.value
      }

    case 'SET_ROOM':
      return {
        ...state,
        roomCode: action.value.roomCode,
        roomName: action.value.roomName,
        roomDescription: action.value.roomDescription,
        roomMode: action.value.roomMode
      }

    case 'SET_FIELD':
      return {
        ...state,
        [action.key]: action.value
      }

    case 'RESET':
      return {
        ...initialState
      }

    default:
      return state
  }
}

export const setEmail = value => ({
  type: 'AUTH_SUCCESS',
  value
})

export const setDefaultLesson = value => ({
  type: 'DEFAULT_LESSON',
  value
})

export const setRoomDetails = value => ({
  type: 'SET_ROOM',
  value
})

export const setField = (key, value) => ({
  type: 'SET_FIELD',
  key,
  value
})

export const resetState = () => ({
  type: 'RESET'
})
