export const COLORS = {
  RED: '#FF7969',
  WHITE: '#FFFFFF',
  GRAY: '#BCBCBC',
  GRADIENT_START: '#FF7969',
  GRADIENT_STOP: '#FFBA6C',
  BLACK: '#666666',
  PURPLE: '#9F8EED',
  GREEN: '#57E06E',
  YELLOW: '#FFB84E',

  BACKGROUND: '#FFFFFF'
}

export const SHADOWS = {
  NAV_BAR: '0px 3px 5px rgba(0, 0, 0, 0.1)',
  CARD: '0px 4px 4px rgba(0, 0, 0, 0.15)'
}

export const BORDER = {
  BUTTON_RADIUS: 24
}

export const FONTS = {
  SIZE: {
    SMALL: 14,
    DEFAULT: 16,
    BIG: 18,
    EXTRA: 36
  }
}

export const LAYOUT_MAX_WIDTH = 1230

export const LAYOUT_PADDINGX = 48

export const LAYOUT_PADDINGY = 36

export default COLORS
