import React from 'react'
import Navbar from './components/Navbar'
import Portal from './pages/portal'
import MainLayout from './components/Layout'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Report from './pages/report'
import Document from './pages/document'
import Rank from './pages/rank'
import Classroom from './pages/classroom'
import Writing from './pages/writing'
import Question from './pages/question'
import Qrcode from './pages/qrcode'
import Manual from './pages/manual'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

export const pathRoute = {
  Report: {
    path: '/report',
    component: Report
  },
  Document: {
    path: '/document',
    component: Document
  },
  Rank: {
    path: '/rank',
    component: Rank
  },
  Classroom: {
    path: '/classroom',
    component: Classroom
  },
  Writing: {
    path: '/writing',
    component: Writing
  },
  Question: {
    path: '/question',
    component: Question
  },
  Qrcode: {
    path: '/qrcode',
    component: Qrcode
  },
  Manual: {
    path: '/manual',
    component: Manual
  }
}

class App extends React.Component {
  state = {
    isSnackbarOpen: false,
    snackbarMessage: ''
  }

  openSnackbarMessage = message => {
    this.setState({ isSnackbarOpen: true, snackbarMessage: message })
  }

  closeSnackbar = () => {
    this.setState({ isSnackbarOpen: false })
  }

  render() {
    const routeWithNavbar = Object.entries(pathRoute).map(([key, route]) => (
      <Route
        key={`route-${route.path}`}
        path={`${route.path}`}
        render={() => {
          return (
            <React.Fragment>
              <Navbar />
              <route.component
                openSnackbarMessage={this.openSnackbarMessage}
                closeSnackbar={this.closeSnackbar}
              />
            </React.Fragment>
          )
        }}
      />
    ))

    return (
      <Router>
        <MainLayout>
          <Switch>
            <Route
              exact={true}
              path="/"
              render={() => (
                <Portal
                  openSnackbarMessage={this.openSnackbarMessage}
                  closeSnackbar={this.closeSnackbar}
                />
              )}
            />
            {routeWithNavbar}
          </Switch>

          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left'
            }}
            open={this.state.isSnackbarOpen}
            autoHideDuration={5000}
            onClose={this.closeSnackbar}
            ContentProps={{
              'aria-describedby': 'message-id'
            }}
            message={<span id="message-id">{this.state.snackbarMessage}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.closeSnackbar}
              >
                <CloseIcon />
              </IconButton>
            ]}
          />
        </MainLayout>
      </Router>
    )
  }
}

export default App
