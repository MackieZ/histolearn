import { COLORS } from './styles'

export const Config = {
  defaultLesson: [
    {
      lesson_no: 1,
      name: 'Irrawaddy Flood Plains',
      detail: 'The Kingdom of Bagan',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson1.pdf?alt=media&token=1476024c-ae7c-44a0-9da6-08331b2e882a',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/1.jpg?alt=media&token=cef12da0-bd99-4bfe-a065-02c4698d3a21'
    },
    {
      lesson_no: 2,
      name: 'Balinese Rice plains',
      detail: 'Religion and Rice',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson2.pdf?alt=media&token=aed78433-f982-49cf-a5f6-1bc038aa2eb2',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/2.jpg?alt=media&token=b1b20ba8-4e33-44fd-81b1-d7f5157b8352'
    },
    {
      lesson_no: 3,
      name: 'Northern Thai Highlands',
      detail: 'The Kingdom of Lanna',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson3.pdf?alt=media&token=df7d9d86-88fa-4857-bd4e-b256628d1a19',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/3.jpg?alt=media&token=60f25019-a3dc-4b94-b0c7-798514a7e36a'
    },
    {
      lesson_no: 4,
      name: 'Highland Houses',
      detail: 'Honai and Tongkonan',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson4.pdf?alt=media&token=dbcc09ce-6954-4f8a-bb8f-d07fe67b4de2',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/4.jpg?alt=media&token=21d427d0-bc6c-459a-88e1-d2c6c78ef4d6'
    },
    {
      lesson_no: 5,
      name: 'West Coast Malay Peninsula',
      detail: 'The Kingdom of Malacca',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson5.pdf?alt=media&token=b97c8b3d-e2d8-40aa-9ce7-ec0c4159e370',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/5.jpg?alt=media&token=94f39140-dd85-45e2-8d71-127564c1e323'
    },
    {
      lesson_no: 6,
      name: 'The Coral Triangle Coastlands',
      detail: 'The Sama/Bajau',
      content:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/lesson6.pdf?alt=media&token=c61710c2-b826-489b-bbc9-e5662395030e',
      mark:
        'https://firebasestorage.googleapis.com/v0/b/histolearn-unesco.appspot.com/o/6.jpg?alt=media&token=70051d2e-bc1f-4008-8b18-d9a6d7a9b3bb'
    }
  ],
  statusRoomColor: {
    save: COLORS.YELLOW,
    waiting: COLORS.PURPLE,
    active: COLORS.GREEN,
    done: COLORS.RED
  },
  defaultLessonQuestionChoice: {
    '0': [],
    '1': [
      {
        answer: 'Buddhism',
        choice1: 'Christianism',
        choice2: 'Hinduism',
        choice3: 'Islam',
        questionName: 'What is the dominant religion of Bagan?'
      },
      {
        answer: 'Semi-desert Plain',
        choice1: 'Valleys',
        choice2: 'Plateau',
        choice3: 'Marine coastal area',
        questionName:
          'What is the Topography ( Physical feature of an area ) of Bagan?'
      },
      {
        answer: 'Ayeyarwady',
        choice1: 'Brahmaputra',
        choice2: 'Ganges',
        choice3: 'Mekong',
        questionName: 'What is the main river crossing Bagan?'
      },
      {
        answer: 'Naturally it cannot support large-scale agriculture.',
        choice1: 'There is no natural water source in the area.',
        choice2: 'People cannot live in this area.',
        choice3: 'There are too many glaciers in the region.',
        questionName:
          'Which of the following statements best describe the Irrawady flood plains near Bagan?'
      },
      {
        answer: 'Agriculture',
        choice1: 'Gold',
        choice2: 'Oil',
        choice3: 'Spices',
        questionName:
          'Thanks to which natural resource did the Kingdom of Bagan become wealth and powerful?'
      },
      {
        answer: 'They commissioned Buddhist temples and religious art.',
        choice1: 'They bought weapons from other countries.',
        choice2: 'They enrolled a large number of soldiers.',
        choice3: 'They developed large markets all over the kingdom.',
        questionName:
          'How did the kings display their wealth and power in the Bagan kingdom?'
      },
      {
        answer: 'Thailand',
        choice1: 'Pakistan',
        choice2: 'Brazil',
        choice3: 'Colombia',
        questionName:
          'In which country is the dominant religion the same as in Bagan?'
      }
    ],
    '2': [
      {
        answer: 'Cutting',
        choice1: 'Shoveling',
        choice2: 'Plowing',
        choice3: 'Pounding',
        questionName: 'What do you use a ani-ani for?'
      },
      {
        answer: 'Garu',
        choice1: 'Ani-ani',
        choice2: 'Lesung',
        choice3: 'Waluku',
        questionName: 'What is the tool used to soften the soil?'
      },
      {
        answer: 'Europe',
        choice1: 'South-East Asia',
        choice2: 'Africa',
        choice3: 'America',
        questionName:
          'In which part of the world don’t people have myths referring to the stealing of rice seeds?'
      },
      {
        answer: 'Cassava',
        choice1: 'Corn',
        choice2: 'Bananas',
        choice3: 'Wheat',
        questionName:
          'According to the folktale “The Goddess of the Rice”, what was the main food of people on the island of Java before they discovered rice?'
      },
      {
        answer:
          'A long time ago, it was only permitted to grow rice in heaven.',
        choice1: 'Humans and Gods never met each other.',
        choice2:
          'Human beings have never been allowed to visit the heaven while they are still alive.',
        choice3:
          'Dewi Sri allowed the farmer to eliminate the birds if they attacked the rice.',
        questionName:
          'According to the folktale “The Goddess of the Rice”, which of the following statements is true?'
      },
      {
        answer: 'Orion',
        choice1: 'Apus',
        choice2: 'Virgo',
        choice3: 'Tucana',
        questionName:
          'Which constellation signals that the season for planting rice has come?'
      },
      {
        answer: 'Hari Bhatari Sri',
        choice1: 'Idul Adha',
        choice2: 'Cuti Bersama',
        choice3: 'Hari Raya Puasa',
        questionName:
          'How is called the special day dedicated to Dewi Sri every month?'
      },
      {
        answer: 'Subak',
        choice1: 'Sukra Umanis',
        choice2: 'Waluku',
        choice3: 'Melanting',
        questionName:
          'What is the name of Traditional irrigational organizations in Bali?'
      },
      {
        answer: 'Orion',
        choice1: 'Lacerta',
        choice2: 'Scutum',
        choice3: 'Virgo',
        questionName: 'What is the other name given to the Waluku star?'
      },
      {
        answer: 'Lesung',
        choice1: 'Garu',
        choice2: 'Waluku',
        choice3: 'Ani-ani',
        questionName: 'How is called a rice mortar in Bali?'
      }
    ],
    '3': [
      {
        answer: 'Alluvial plain and mountain',
        choice1: 'Alluvial plain',
        choice2: 'Mountain',
        choice3: 'Semi-desert plain',
        questionName:
          'What is the topography (physical feature of an area) of the Kingdom of Lanna?'
      },
      {
        answer: 'Northern Thailand',
        choice1: 'Southern Thailand',
        choice2: 'Eastern Thailand',
        choice3: 'Western Thailand',
        questionName:
          'In which part of nowadays Thailand was the Lanna Kingdom located?'
      },
      {
        answer: '600 meters above sea level',
        choice1: '300 meters above sea level',
        choice2: '500 meters above sea level',
        choice3: '1,000 meters above sea level',
        questionName:
          'What is the average elevation of the alluvial plains of Lanna?'
      },
      {
        answer: 'The largest settlements developed in the mountainous areas.',
        choice1: 'Tai speakers were the dominant group.',
        choice2: 'Tai speakers lived primarily in the alluvial plains.',
        choice3:
          'In the mountainous region, Buddhism was not the main religion.  ',
        questionName:
          'Which of the following statements is not true regarding the Kingdom of Lanna?'
      },
      {
        answer: 'Tai speakers in Northern Thailand',
        choice1: 'Chinese writers',
        choice2: 'Missionaries',
        choice3: 'Middle East merchants',
        questionName:
          'Which group of people have transmitted knowledge about the Lanna period?'
      },
      {
        answer:
          'Buddhism was the main religion practiced in the alluvial plains.',
        choice1:
          'Historians date the Kingdom of Lanna from between the eighteenth and twentieth century.',
        choice2:
          'Farms are larger in the highlands, while small-scale agriculture was developed in the alluvial plains.',
        choice3:
          'The mountainous region is at least 2.000 meters above sea level.',
        questionName:
          'Which of the following statements is true regarding the Kingdom of Lanna?'
      },
      {
        answer: 'Iron',
        choice1: 'Ammunition',
        choice2: 'Oil',
        choice3: 'Spices',
        questionName:
          'Which of the following products was traded by the inhabitants of the mountainous areas of Lanna?'
      }
    ],
    '4': [
      {
        answer: 'Tongkonan',
        choice1: 'Tangdo',
        choice2: 'Sali',
        choice3: 'Sumbung',
        questionName: 'What is the name of the traditional Torajan house?'
      },
      {
        answer: 'Indonesia',
        choice1: 'Thailand',
        choice2: 'Cambodia',
        choice3: 'Vietnam',
        questionName: 'In which country do the Torajan people live?'
      },
      {
        answer: 'The sleeping quarters for unmarried women',
        choice1: 'The cooking area',
        choice2: 'The fireplace area',
        choice3: 'The room where the family prepares the funerals',
        questionName:
          'In the tongkonan, what is the function of the northern room called the tangdo?'
      },
      {
        answer:
          'The sumbung is a room located in the house’s northern side and used to accommodate guests.',
        choice1: 'Tongkonans are built with a large saddleback roof.',
        choice2:
          'Tongkonans are built facing North towards the source of the rivers.',
        choice3: 'The Sali is used as the family’s living and cooking space.',
        questionName: 'Which of the following statements is not true? '
      },
      {
        answer: 'July - September',
        choice1: 'January - March',
        choice2: 'April - June',
        choice3: 'October - December',
        questionName: 'When will the torajan people hold the funeral ceremony?'
      },
      {
        answer: 'Honai',
        choice1: 'Torajan',
        choice2: 'Tongkonan',
        choice3: 'Sumbung',
        questionName:
          'What is the name of the traditional house of people in the highlands of Wamena, in West Papua?'
      },
      {
        answer: 'A honai is divided into 5 floors.',
        choice1: 'Wamena is located in West Papua, Indonesia.',
        choice2: 'A wamai is a type of honai used for housing pigs.',
        choice3: 'Honai are about 2.5 meters high.',
        questionName: 'Which of the following statements is not true?'
      },
      {
        answer: 'A space used by women.',
        choice1: 'A space used by men.',
        choice2: 'A space used to raise pigs.',
        choice3: 'A space used to keep to the bodies of deceased people.',
        questionName: 'What is the function of a ebei?'
      },
      {
        answer: 'Honai, Ebai, Wamai',
        choice1: 'Honai, Ebai, Sali',
        choice2: 'Tangdo, Sali, Sumbung',
        choice3: 'Sumbung, Ebai, Tongkonan',
        questionName: 'What are the 3 types of honai?'
      },
      {
        answer: 'To light a fire',
        choice1: 'To raise pigs',
        choice2: 'To sleep',
        choice3: 'To eat and relax',
        questionName: 'What is the centre of the honai commonly used for?'
      }
    ],
    '5': [
      {
        answer: 'West',
        choice1: 'North',
        choice2: 'South',
        choice3: 'East',
        questionName:
          'On which side of the Malacca Straits was the kingdom of Malacca located?'
      },
      {
        answer: 'India and China',
        choice1: 'Malaysia and Indonesia',
        choice2: 'India and Thailand',
        choice3: 'China and Malaysia',
        questionName:
          'Malacca developed as a trading centre to service exchanges between which of the following countries?'
      },
      {
        answer: 'They live on the sea.',
        choice1: 'They live in the wild forests.',
        choice2: 'They live very high in the mountains.',
        choice3: 'They are very skilled gardeners.',
        questionName: 'Which is the specificity of orang laut people?'
      },
      {
        answer: 'In the forested areas outside Malacca.',
        choice1: 'On the sea.',
        choice2: 'In the high mountains.',
        choice3: 'In the city of Malacca.',
        questionName: 'Where do the orang asli people live?'
      },
      {
        answer: 'Portugal',
        choice1: 'Netherland',
        choice2: 'Great Britain',
        choice3: 'France',
        questionName:
          'Which country conquered the kingdom of Malacca in the 16th century?'
      },
      {
        answer: 'Between Malaysia and Sumatra island',
        choice1: 'Between Persian Gulf and the Gulf of Oman',
        choice2: 'Betwenn Russia and the United States',
        choice3: 'Between the Indonesian islands of Java and Sumatra.',
        questionName: 'Where was the Malacca Straits?'
      },
      {
        answer: 'The large scale agriculture can be found easily in Malacca.',
        choice1:
          'The rulers of Malacca believe that the best way to attract followers is by being wealthy.',
        choice2: 'Fishing was a major occupation among the men.',
        choice3: 'Malacca is generally considered a Malay kingdom.',
        questionName: 'Which of the following about Malacca is not true?'
      },
      {
        answer: 'Both Orang asli and Orang laut are not Malays',
        choice1: 'Orang asli',
        choice2: 'Orang laut',
        choice3: 'Both Orang asli and Orang laut are Malays',
        questionName: 'Which of the following groups is not Malays?'
      },
      {
        answer: 'Russian',
        choice1: 'Chinese',
        choice2: 'Indian',
        choice3: 'Persian',
        questionName:
          'There were large groups of foreigners living in Malacca. Which of the following was not?'
      },
      {
        answer: '1511',
        choice1: '1509',
        choice2: '1525',
        choice3: '1599',
        questionName: 'What year the malacca was conquered?'
      }
    ],
    '6': [
      {
        answer: 'Australia',
        choice1: 'Philippines',
        choice2: 'Malaysia',
        choice3: 'Papua New Guinea',
        questionName: 'Which country is not in the coral triangle area?'
      },
      {
        answer: 'Islam',
        choice1: 'Buddhism',
        choice2: 'Christianism',
        choice3: 'Hinduism',
        questionName: 'What is the main religion practiced by the Bajau people?'
      },
      {
        answer: 'A boat',
        choice1: 'A house',
        choice2: 'The sea',
        choice3: 'Fishing equipment',
        questionName: 'What is a sakai?'
      },
      {
        answer: '5.5',
        choice1: '4.5',
        choice2: '6.5',
        choice3: '7.5',
        questionName:
          'What is the size of the area known as the coral triangle? (in million square kilometers)?'
      },
      {
        answer: 'Orang asli',
        choice1: 'Orang Laut',
        choice2: 'Sama di Laut',
        choice3: 'Sea Nomads',
        questionName: 'Bajau people are known by many names. Which of the following names does not refer to them?'
      },
      {
        answer: 'The Western Pacific and Indian oceans',
        choice1: 'The Western North Atlantic ocean',
        choice2: 'The Southern Arctic ocean',
        choice3: 'The Northern Pacific ocean',
        questionName: 'In which ocean(s) is located the area known as the Coral Triangle?'
      },
      {
        answer:
          'Two of the world’s seven species of sea turtles live in the area.',
        choice1: 'There are over 600 different species of reef building corals.',
        choice2:
          'It contains the largest proportion of mangrove forests on the planet.',
        choice3: 'It spans over 1.5 million square kilometers of land.',
        questionName:
          'Which of the following statements about the Coral Triangle is not true?'
      },
      {
        answer: 'Thailand',
        choice1: 'Philippines',
        choice2: 'Malaysia',
        choice3: 'Indonesia',
        questionName:
          'The seafaring people known as the "Bajau", “Sama”, or "Orang Laut" are found in several areas of South-East Asia. In which country are they not sailing?'
      },
      {
        answer:
          'The Bajau’ bodies adapt to traditional fishing methods, which includes deep diving, from a young age.',
        choice1:
          'The Bajau depend on modern fishing methods for their food and livelihoods.',
        choice2:
          'Experienced Bajau divers can reach depths of 10 m below the ocean surface on one breath.',
        choice3:
          'Studies have shown that the eyesight of Bajau children in underwater environments is as sharp as the eyesight of children who live on land.',
        questionName: 'Which of the following statement is true?'
      },
      {
        answer: 'The sea spirits of the ancestors',
        choice1: 'A Bajau’s boat',
        choice2: 'A small region in the Coral Triangle',
        choice3: 'Bajau’s modern fishing equipment',
        questionName: 'What is the Mbo Madilau?'
      }
    ]
  },
  defaultLessonQuestionReplies: {
    '0': [],
    '1': [
      {
        questionName:
          'Could irrigation systems be constructed everywhere in the dry zone around Bagan? Why? 
      },
      {
        questionName:
          'Why did rice cultivation and religion become so important in the Bagan dry zone?'
      }
    ],
    '2': [
      {
        questionName:
          'How do religion and cosmology influence the behavior of Balinese people?'
      },
      {
        questionName: `How do people's beliefs influence rice cultivation and food production?`
      }
    ],
    '3': [
      {
        questionName:
          'Life in the mountains is often difficult. Why did people chose to settle down in this harsh environment?'
      },
      {
        questionName:
          'How did people access food and essential items in the mountainous regions of the Lanna kingdom?'
      }
    ],
    '4': [
      {
        questionName: 'Why did Papuan people chose the design their houses, the honai, the way they are? '
      },
      {
        questionName: `How are the daily activities organized in the honai?`
      }
    ],
    '5': [
      {
        questionName:
          'When Malacca was a trading centre, people from different countries, religions and cultures live and work together. What motivated them to interact peacefully?'
      },
      {
        questionName:
          'What factors impacted the success of Malacca as a trading centre?'
      }
    ],
    '6': [
      {
        questionName:
          'What are the Bajau people doing to help preserve the environment, and in particular the sea?'
      },
      {
        questionName:
          'Sea people (Bajau) and land people think and act differently about the ocean. What are the differences?'
      }
    ]
  }
}

export default Config
