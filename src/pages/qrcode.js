import React from 'react'
import Content from '../components/Content'
import styled from 'styled-components'
import RoomDetails from '../components/RoomDetails'
import { WrapperCol } from './rank'
import Button from '../components/Button'
import QRCode from 'qrcode.react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { FONTS } from '../styles'
import firebase from 'firebase'
import moment from 'moment'
import axios from 'axios'
import { pathRoute } from '../App'

const ButtonGroupWrapper = styled.div`
  max-width: 460px
  margin-left: auto;
  margin-right: auto;
  margin-top: 1.5rem;
  display: flex;
  justify-content: space-around;
  align-items: center;
`

const QrcodeWrapper = styled.div`
  padding: 1.5rem;
  display: flex;
  justify-content: center;
  align-items: center;
`

const JoinedWrapper = styled.div`
  font-size: ${FONTS.SIZE.BIG}px;
  text-align: right;
  font-weight: 500;
  line-height: 2;
`

const Student = styled.div`
  font-size: ${FONTS.SIZE.BIG}px;
  text-align: right;
`

class Qrcode extends React.Component {
  state = {
    students: {}
  }
  componentDidMount() {
    firebase
      .database()
      .ref('rooms/' + this.props.roomCode + '/students')
      .on('value', snap => {
        this.setState({
          students: snap.val() || this.state.students
        })
      })
  }

  handlerButtonClick = () => {
    const { roomCode, roomMode, roomTime } = this.props

    axios
      .get('https://us-central1-histolearn-unesco.cloudfunctions.net/getTime')
      .then(({ data }) => {
        const timeFinish = moment(data)
          .add(+roomTime, 'minutes')
          .unix('X')

        firebase
          .database()
          .ref('rooms/' + roomCode + '/timeEnd')
          .set(timeFinish)

        firebase
          .database()
          .ref('rooms/' + roomCode + '/status')
          .set('active')

        this.props.history.push(
          roomMode === 'choice' ? pathRoute.Rank.path : pathRoute.Writing.path
        )
      })
  }

  render() {
    const props = this.props
    const students = Object.keys(this.state.students).map(student => (
      <Student key={`student-${student}`}>{student}</Student>
    ))
    return (
      <Content>
        <WrapperCol width={'20%'} />
        <WrapperCol width={'60%'}>
          <RoomDetails
            code={props.roomCode}
            name={props.roomName}
            description={props.roomDescription}
            time={props.roomTime}
          />
          <QrcodeWrapper>
            <QRCode value={props.roomCode || 'Histolearn'} size={324} />
          </QrcodeWrapper>
          <ButtonGroupWrapper>
            <Button onClick={this.handlerButtonClick}>Start</Button>
          </ButtonGroupWrapper>
        </WrapperCol>
        <WrapperCol width={'20%'}>
          <JoinedWrapper>Student joined: </JoinedWrapper>
          {students}
        </WrapperCol>
      </Content>
    )
  }
}

const mapStateToProps = ({
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
}) => ({
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
})

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Qrcode)
)
