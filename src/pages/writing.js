import React from 'react'
import Content from '../components/Content'
import { Tabs } from 'antd'
import styled from 'styled-components'
import { TabWrapper } from './classroom'
import { COLORS, FONTS, SHADOWS } from '../styles'
import RoomDetails from '../components/RoomDetails'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import firebase from 'firebase'
import moment from 'moment'
import { ButtonOverride } from '../components/Button'

const Question = styled.div`
  font-weight: 600;
  margin-bottom: 1rem;
`

const CardWrapper = styled.div`
  padding: 4px;
  width: 100%;
  column-width: 300px;
  column-gap: 50px;

  > div {
    -webkit-column-break-inside: avoid;
    page-break-inside: avoid;
    break-inside: avoid;
    margin-bottom: 2rem;
  }
`

const Card = styled.div`
  padding: 2rem;
  box-shadow: ${SHADOWS.CARD};
`

const CardTitle = styled.div`
  color: ${COLORS.RED};
  font-size: ${FONTS.SIZE.BIG}px;
  margin: 5px 0;
`

const CardDetailsWrapper = styled.div`
  word-break: break-all;
`

const TimerWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const WrapperCol = styled.div`
  width: ${props => props.width || '33.33%'};
  display: inline-block;
  vertical-align: top;
  position: relative;
`

const TabPane = Tabs.TabPane

const Time = styled.span`
  margin-right: 15px;
  font-weight: 500;
  letter-spacing: 2px;
`

class Writing extends React.Component {
  state = {
    questions: {},
    trigger: true,
    timer: 0
  }

  componentDidMount() {
    const { roomCode } = this.props
    firebase
      .database()
      .ref('rooms/' + roomCode)
      .on('value', data => {
        if (data.val()) {
          this.setState({ questions: data.val().questions || {} })
        }
      })

    firebase
      .database()
      .ref('rooms/' + roomCode)
      .once('value', snap => {
        const val = snap.val() || {}
        const timeEnd = moment.unix(val.timeEnd)
        const duration = moment.duration(timeEnd.diff(moment()))

        this.setState(
          {
            timer: ~~duration.asSeconds() >= 0 ? ~~duration.asSeconds() : 0,
            ...val
          },
          () => this.state.trigger && this.timeCount()
        )
      })
  }

  timeCount = () => {
    this.setState(
      { trigger: false },
      () => (this.counter = setInterval(this.countDown, 1000))
    )
  }

  countDown = () => {
    const sec = this.state.timer - 1
    if (sec <= 0) {
      clearInterval(this.counter)
      this.setState(
        {
          timer: 0
        },
        () => {
          firebase
            .database()
            .ref('rooms/' + this.props.roomCode + '/status')
            .set('done')
        }
      )
    } else {
      this.setState({
        timer: sec
      })
    }
  }

  handlerTimeStop = () => {
    const { roomCode } = this.props
    firebase
      .database()
      .ref('rooms/' + roomCode + '/status')
      .set('done')
      .then(() => {
        firebase
          .database()
          .ref('rooms/' + roomCode)
          .once('value', () => {
            const timeEnd = moment().unix('X')
            firebase
              .database()
              .ref('rooms/' + roomCode + '/timeEnd')
              .set(timeEnd)
              .then(() => {
                this.setState({ timer: 0 })
              })
          })
      })
  }

  render() {
    const { roomName, roomDescription } = this.props
    const { timer } = this.state

    const questions = Object.values(this.state.questions).map(
      (question, index) => (
        <TabPane tab={`Question ${index + 1}`} key={index}>
          <Question>
            Question {index + 1}: {question.questionName}
          </Question>
          <CardWrapper>
            {!!question.replies &&
              Object.keys(question.replies).map((student, index) => {
                return (
                  <Card key={`${index}`}>
                    <CardTitle>{student}</CardTitle>
                    <CardDetailsWrapper>
                      {question.replies[student]}
                    </CardDetailsWrapper>
                  </Card>
                )
              })}
          </CardWrapper>
        </TabPane>
      )
    )

    return (
      <Content>
        <WrapperCol width={'25%'} />
        <WrapperCol width={'50%'}>
          <RoomDetails name={roomName} description={roomDescription} />
        </WrapperCol>
        <WrapperCol width={'25%'}>
          <TimerWrapper>
            <Time>{`${
              ~~(timer / 60) > 9 ? `${~~(timer / 60)}` : `0${~~(timer / 60)}`
            } : ${
              ~~(timer % 60) <= 9 ? `0${~~(timer % 60)}` : ~~(timer % 60)
            } `}</Time>

            {timer !== 0 ? (
              <ButtonOverride onClick={this.handlerTimeStop}>
                Stop
              </ButtonOverride>
            ) : (
              ''
            )}
          </TimerWrapper>
        </WrapperCol>
        <TabWrapper>
          <Tabs onChange={this.onChangeTabs} type="card">
            {questions}
          </Tabs>
        </TabWrapper>
      </Content>
    )
  }
}

const mapStateToProps = ({
  defaultLesson,
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
}) => ({
  defaultLesson,
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
})

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Writing)
)
