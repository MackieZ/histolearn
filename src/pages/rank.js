import React from 'react'
import Content from '../components/Content'
import { Title } from './report'
import styled from 'styled-components'
import { COLORS, FONTS } from '../styles'
import RoomDetails from '../components/RoomDetails'
import { ButtonOverride } from '../components/Button'
import RankTrophy from '../assets/rank.svg'
import firebase from 'firebase'
import moment from 'moment'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { pathRoute } from '../App'

export const WrapperCol = styled.div`
  width: ${props => props.width || '33.33%'};
  display: inline-block;
  vertical-align: top;
  position: relative;
`

const RankingWrapper = styled.div`
  margin-top: 1rem;
  font-size: ${FONTS.SMALL}px;
  line-height: 1.5;
`
const RankBox = styled.div``

const RankNumber = styled.span`
  color: ${COLORS.RED};
  margin-right: 8px;
`

const TimerWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const Time = styled.span`
  margin-right: 15px;
  font-weight: 500;
  letter-spacing: 2px;
`

const Trophy = styled.img`
  margin-top: 80px;
  margin-left: -130px;
  width: 844px;
  height: 496px;
  position: relative;
`

const Top1 = styled.div`
  font-size: ${FONTS.SIZE.EXTRA}px;
  font-weight: 600;

  bottom: 450px;
  left: 195px;
  position: absolute;
  text-align: center;
  width: 200px;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Top2 = styled.div`
  font-size: ${FONTS.SIZE.BIG}px;
  font-weight: 500;

  bottom: 370px;
  left: 45px;
  position: absolute;
  text-align: center;
  width: 120px;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Top3 = styled.div`
  font-size: ${FONTS.SIZE.DEFAULT}px;
  font-weight: 500;

  bottom: 340px;
  right: 0px;
  position: absolute;
  text-align: center;
  width: 130px;
  overflow: hidden;
  text-overflow: ellipsis;
`

class Rank extends React.Component {
  state = {
    timer: 0,
    trigger: true,
    students: {},
    studentsRank: [],
    questions: []
  }

  constructor() {
    super()
    this.counter = 0
  }

  componentDidMount() {
    const { roomCode } = this.props

    firebase
      .database()
      .ref('rooms/' + roomCode)
      .once('value', snap => {
        const val = snap.val() || {}
        const timeEnd = moment.unix(val.timeEnd)
        const duration = moment.duration(timeEnd.diff(moment()))

        this.setState(
          {
            timer: ~~duration.asSeconds() >= 0 ? ~~duration.asSeconds() : 0,
            ...val
          },
          () => this.state.trigger && this.timeCount()
        )
      })

    firebase
      .database()
      .ref('rooms/' + roomCode + '/students')
      .on('value', snap => {
        let students = snap.val()
        students &&
          Object.keys(students).sort((a, b) => {
            return b.score - a.score
          })

        let studentsRank = []

        students &&
          Object.keys(students).map((student, index) => {
            students[student].rank = index + 1
            studentsRank[index] = { ...students[student] }
          })

        this.setState({ students: students || {}, studentsRank }, () => {
          firebase
            .database()
            .ref('rooms/' + roomCode + '/students')
            .set(students)
        })
      })
  }

  timeCount = () => {
    this.setState(
      { trigger: false },
      () => (this.counter = setInterval(this.countDown, 1000))
    )
  }

  countDown = () => {
    const { roomCode } = this.props

    const sec = this.state.timer - 1
    if (sec <= 0) {
      clearInterval(this.counter)
      this.setState(
        {
          timer: 0
        },
        () => {
          firebase
            .database()
            .ref('rooms/' + roomCode + '/status')
            .set('done')
        }
      )
    } else {
      this.setState({
        timer: sec
      })
    }
  }

  handlerTimeStop = () => {
    const { roomCode } = this.props
    firebase
      .database()
      .ref('rooms/' + roomCode + '/status')
      .set('done')
      .then(() => {
        firebase
          .database()
          .ref('rooms/' + roomCode)
          .once('value', () => {
            const timeEnd = moment().unix('X')
            firebase
              .database()
              .ref('rooms/' + roomCode + '/timeEnd')
              .set(timeEnd)
              .then(() => {
                this.setState({ timer: 0 }, () =>
                  this.props.history.push(pathRoute.Report.path)
                )
              })
          })
      })
  }

  render() {
    const rank = this.state.studentsRank
    const { studentsRank, timer } = this.state
    const { roomCode, roomName, roomDescription, roomTime } = this.props

    const maxScore = this.state.questions.length

    const ranks = rank.map((rank, index) => (
      <RankBox key={`rank-${index}`}>
        <RankNumber>{index + 1}.</RankNumber>
        {rank.name} - {rank.score}/{maxScore}
      </RankBox>
    ))

    return (
      <Content>
        <WrapperCol width={'25%'}>
          <Title>Ranking</Title>
          <RankingWrapper>{ranks}</RankingWrapper>
        </WrapperCol>
        <WrapperCol width={'50%'}>
          <RoomDetails
            code={roomCode}
            name={roomName}
            description={roomDescription}
            time={roomTime}
          />
          <Trophy src={RankTrophy} />
          <Top1>
            {studentsRank.length > 0
              ? studentsRank[0] && studentsRank[0].name
              : '-'}
          </Top1>
          <Top2>
            {studentsRank.length > 0
              ? studentsRank[1] && studentsRank[1].name
              : '-'}
          </Top2>
          <Top3>
            {studentsRank.length > 0
              ? studentsRank[2] && studentsRank[2].name
              : '-'}
          </Top3>
        </WrapperCol>
        <WrapperCol width={'25%'}>
          <TimerWrapper>
            <Time>{`${
              ~~(timer / 60) > 9 ? `${~~(timer / 60)}` : `0${~~(timer / 60)}`
            } : ${
              ~~(timer % 60) <= 9 ? `0${~~(timer % 60)}` : ~~(timer % 60)
            } `}</Time>

            <ButtonOverride onClick={this.handlerTimeStop}>
              {timer !== 0 ? 'Stop' : 'Details'}
            </ButtonOverride>
          </TimerWrapper>
        </WrapperCol>
      </Content>
    )
  }
}

const mapStateToProps = ({
  roomName,
  roomDescription,
  roomTime,
  roomCode
}) => ({
  roomName,
  roomDescription,
  roomTime,
  roomCode
})

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Rank)
)
