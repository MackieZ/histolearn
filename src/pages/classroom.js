import React from 'react'
import Content from '../components/Content'
import { Tabs } from 'antd'
import styled from 'styled-components'
import { COLORS, FONTS } from '../styles'
import { ButtonOverride } from '../components/Button'
import Config from '../config'
import { RoomBox } from '../components/RoomBox'
import Modal from '../components/Modal'
import Input from '../components/Input'
import Button from '../components/Button'
import { pathRoute } from '../App'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'

import {
  setDefaultLesson,
  setRoomDetails,
  resetState,
  setField
} from '../reducer'
import firebase from 'firebase'

const styles = {
  defaultClassroomButton: {
    default: {
      borderRadius: 5,
      background: 'white',
      border: `1px solid ${COLORS.GRAY}`,
      color: '#666666',
      display: 'inline-block',
      padding: '.5rem 2rem'
    },
    inActive: {
      borderRadius: 5
    }
  }
}

const TabPane = Tabs.TabPane

export const TabWrapper = styled.div`
  .ant-tabs {
    font-family: 'Roboto', sans-serif;
    font-size: 1rem;
    color: #666666;
  }

  .ant-tabs-tab {
    &:hover {
      color: ${COLORS.RED} !important;
    }
  }

  .ant-tabs.ant-tabs-card > .ant-tabs-bar .ant-tabs-tab-active {
    color: ${COLORS.RED};
  }
`

const RoomWrapper = styled.div`
  margin-top: 2rem;
`

const ModalInputBox = styled.div`
  display: flex;
  align-items: center;

  margin-bottom: 1rem;
  margin: ${props => props.margin};
`

const ModalInputLabel = styled.div`
  min-width: 25%;
  text-align: right;
  margin-right: 1rem;
`

const ButtonGroupWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
`

const ModalDefaultClassroomWrapper = styled.div``

const ModalDefaultClassroomCard = styled.div`
  display: inline-block;
  vertical-align: top;
  width: 33%;
  height: 120px;
  text-align: center;
  padding: 1rem;
  border: 0.5px solid ${COLORS.GRAY};
  border-radius: 3px;
  word-break: break-word;
  cursor: pointer;
`

const ModalDefaultClasroomTitle = styled.div`
  font-size: ${FONTS.SIZE.BIG}px;
  padding: 0.5rem;
  font-weight: 600;
`

class Classroom extends React.Component {
  state = {
    isModalOpen: false,
    roomName: '',
    roomDescription: '',
    rooms: {},
    mode: 'choice',
    isDefault: true
  }

  componentDidMount() {
    const { resetState } = this.props

    resetState()

    firebase.auth().onAuthStateChanged(user => {
      firebase
        .database()
        .ref('rooms')
        .orderByChild('owner')
        .equalTo(user.uid)
        .on('value', snap => {
          const rooms = snap.val()
          this.setState({ rooms: rooms || {} })
        })
    })
  }

  onChangeTabs = () => {}

  handleModal = (isModalOpen, mode) => {
    this.setState({ isModalOpen, mode })
  }

  onClickNewClassroom = () => {
    const { setRoomDetails, openSnackbarMessage } = this.props
    const { roomName, roomDescription, mode } = this.state

    if (!roomName || !roomDescription) {
      return openSnackbarMessage('Please fill all the blank field')
    }

    setRoomDetails({ roomName, roomDescription, roomMode: mode })
    this.props.history.push(pathRoute.Question.path)
  }

  onClickRoomBox = room => {
    const statusPathConfig = {
      save: [pathRoute.Question.path, pathRoute.Question.path],
      waiting: [pathRoute.Qrcode.path, pathRoute.Qrcode.path],
      active: [pathRoute.Rank.path, pathRoute.Writing.path],
      done: [pathRoute.Report.path, pathRoute.Writing.path]
    }

    const { setField } = this.props

    const { status, mode, code, name, description } = room
    const index = mode === 'choice' ? 0 : 1

    setField('roomName', name)
    setField('roomDescription', description)
    setField('roomCode', code)
    setField('roomMode', mode)
    this.props.history.push(statusPathConfig[status][index])
  }

  handleInput = (key, e) => {
    this.setState({ [key]: e.target.value })
  }

  onClickDefaultLesson = (lesson_no, mode) => {
    console.log(lesson_no, mode)
    const { setRoomDetails, setDefaultLesson } = this.props

    setDefaultLesson(lesson_no)
    setRoomDetails({
      roomName: Config.defaultLesson[lesson_no - 1].name,
      roomDescription: Config.defaultLesson[lesson_no - 1].detail,
      roomMode: mode
    })
    this.props.history.push(pathRoute.Question.path)
  }

  onDeleteRoomBox = (code, event) => {
    event.stopPropagation()
    const { openSnackbarMessage } = this.props
    firebase
      .database()
      .ref('rooms/' + code)
      .remove()
      .then(() => {
        openSnackbarMessage(`Remove ${code} Success`)
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  render() {
    const { roomName, roomDescription } = this.state

    const roomsChoiceBox = Object.values(this.state.rooms).map(room => {
      if (room.mode !== 'choice') {
        return null
      }

      return (
        <RoomBox
          key={`room-${room.code}`}
          onClick={event => this.onClickRoomBox(room)}
          code={room.code}
          name={room.name}
          description={room.description}
          status={room.status}
          onDelete={event => this.onDeleteRoomBox(room.code, event)}
        />
      )
    })

    const roomsRepliesBox = Object.values(this.state.rooms).map(room => {
      if (room.mode === 'choice') {
        return null
      }

      return (
        <RoomBox
          key={`room-${room.code}`}
          onClick={event => this.onClickRoomBox(room)}
          code={room.code}
          name={room.name}
          description={room.description}
          status={room.status}
          onDelete={event => this.onDeleteRoomBox(room.code, event)}
        />
      )
    })

    const roomsChoice = (
      <RoomWrapper>
        <RoomBox
          addClassroom
          onClick={() => this.handleModal(true, 'choice')}
        />
        {roomsChoiceBox}
      </RoomWrapper>
    )

    const roomsReplies = (
      <RoomWrapper>
        <RoomBox addClassroom onClick={() => this.handleModal(true, 'reply')} />
        {roomsRepliesBox}
      </RoomWrapper>
    )

    const modalDefaultClassroom = Config.defaultLesson.map(
      ({ lesson_no, name }) => (
        <ModalDefaultClassroomCard
          key={`lesson-${lesson_no}`}
          onClick={() => this.onClickDefaultLesson(lesson_no, this.state.mode)}
        >
          <ModalDefaultClasroomTitle>
            Lesson {lesson_no}
          </ModalDefaultClasroomTitle>
          {name}
        </ModalDefaultClassroomCard>
      )
    )

    return (
      <Content>
        <TabWrapper>
          <Tabs onChange={this.onChangeTabs} type="card">
            <TabPane tab="Multiple Choice" key="1">
              {roomsChoice}
            </TabPane>
            <TabPane tab="Reflections and opinions" key="2">
              {roomsReplies}
            </TabPane>
          </Tabs>
          <Modal
            onClose={() => this.handleModal(false)}
            title={`Create New Room (${
              this.state.mode === 'choice'
                ? 'Multiples choice'
                : 'Reflections and opinions'
            })`}
            open={this.state.isModalOpen}
          >
            <ButtonGroupWrapper>
              <ButtonOverride
                className="button-hover"
                width={'48%'}
                {...styles.defaultClassroomButton[
                  this.state.isDefault ? 'inActive' : 'default'
                ]}
                onClick={() => this.setState({ isDefault: true })}
              >
                Default
              </ButtonOverride>
              <ButtonOverride
                className="button-hover"
                width={'48%'}
                {...styles.defaultClassroomButton[
                  !this.state.isDefault ? 'inActive' : 'default'
                ]}
                onClick={() => this.setState({ isDefault: false })}
              >
                New
              </ButtonOverride>
            </ButtonGroupWrapper>
            {this.state.isDefault ? (
              <ModalDefaultClassroomWrapper>
                {modalDefaultClassroom}
              </ModalDefaultClassroomWrapper>
            ) : (
              <React.Fragment>
                <ModalInputBox>
                  <ModalInputLabel>Room name : </ModalInputLabel>
                  <Input
                    placeholder={'Room name'}
                    onChange={e => this.handleInput('roomName', e)}
                    value={roomName}
                  />
                </ModalInputBox>
                <ModalInputBox margin={'0 0 1.5rem'}>
                  <ModalInputLabel>Details : </ModalInputLabel>
                  <Input
                    placeholder={'Room detail'}
                    onChange={e => this.handleInput('roomDescription', e)}
                    value={roomDescription}
                  />
                </ModalInputBox>
                <Button onClick={this.onClickNewClassroom}>Submit</Button>
              </React.Fragment>
            )}
          </Modal>
        </TabWrapper>
      </Content>
    )
  }
}

const mapStateToProps = ({ defaultLesson }) => ({ defaultLesson })
const mapStateToDispatch = {
  setDefaultLesson,
  setRoomDetails,
  resetState,
  setField
}

export default withRouter(
  connect(
    mapStateToProps,
    mapStateToDispatch
  )(Classroom)
)
