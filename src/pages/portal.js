import React from 'react'
import { MainLayout } from '../components/Layout'
import styled from 'styled-components'
import { COLORS } from '../styles'
import PortalBG from '../assets/login-bg.png'
import MainLogo from '../assets/logo.png'
import Input from '../components/Input'
import Email from '@material-ui/icons/Email'
import Lock from '@material-ui/icons/Lock'
import Button from '../components/Button'
import Modal from '../components/Modal'
import { pathRoute } from '../App'
import firebase from 'firebase'
import { withRouter } from 'react-router-dom'

const FullLayout = MainLayout.extend`
  background: ${COLORS.WHITE};
  background-image: url(${PortalBG});
  background-repeat: no-repeat, repeat;
  background-position: center;
  // background-size: cover;
  min-height: 0;
  height: 100vh;
  height: -webkit-fill-available;
  max-height: 800px;
  max-width: 1560px;
  position: relative;
`

const BoxWrapper = styled.div`
  position: absolute;
  right: 115px;
  top: 92px;
  max-width: 450px;
`

const Logo = styled.img`
  width: 100%;
  position: relative;
  height: 120px;
`

const FieldWrapper = styled.div`
  padding: 0.5rem 1.75rem 0.5rem 2.5rem;
`

const InputFieldWrapper = styled.div`
  margin-bottom: 1rem;
  margin: ${props => props.margin};
`

const PropertiesField = styled.div`
  margin-top: 1.75rem;
`

const Info = styled.div`
  display: inline-block;
  vertical-align: center;
  margin-left: 1.5rem;
`

const Register = styled.div`
  display: inline-block;
  vertical-align: center;
  color: ${COLORS.RED};
  padding: 0 0.5rem;
  cursor: pointer;
`

const ForgotPassword = styled.div`
  display: inline-block;
  vertical-align: center;
  border-left: 1px solid ${COLORS.GRAY};
  color: ${COLORS.GRAY};
  padding: 0 0.5rem;
  cursor: pointer;
`

class Portal extends React.Component {
  state = {
    isModalOpen: false,
    isRegister: false,
    email: '',
    password: ''
  }

  handleModal = isModalOpen => {
    this.setState({ isModalOpen })
  }

  onChangeInput = (key, e) => {
    this.setState({ [key]: e.target.value })
  }

  handleRegisterState = isRegister => {
    this.setState({ isRegister })
  }

  onClickSignIn = () => {
    const { email, password } = this.state
    const { openSnackbarMessage } = this.props

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.props.history.push(pathRoute.Classroom.path)
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  onClickRegister = () => {
    const { email, password } = this.state
    const { openSnackbarMessage } = this.props

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.props.history.push(pathRoute.Classroom.path)
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  onSubmitForgotPassword = () => {
    const { email } = this.state
    const { openSnackbarMessage } = this.props
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        openSnackbarMessage('Password reset has sent to your email')
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  render() {
    const { isModalOpen, isRegister, email, password } = this.state

    const CustomModal = (
      <Modal
        onClose={() => this.handleModal(false)}
        open={isModalOpen}
        title={'Forgot Password ?'}
        subTitle={'Please enter your email to create new password'}
      >
        <InputFieldWrapper margin={'1rem 0 1.5rem'}>
          <Input
            placeholder={'Email'}
            prefixicon={<Email />}
            value={email}
            onChange={e => this.onChangeInput('email', e)}
          />
        </InputFieldWrapper>
        <Button onClick={() => this.onSubmitForgotPassword()}>Submit</Button>
      </Modal>
    )

    return (
      <FullLayout>
        <BoxWrapper>
          <Logo src={MainLogo} />
          <form
            onSubmit={e => {
              e.preventDefault()
              isRegister ? this.onClickRegister() : this.onClickSignIn()
            }}
          >
            <FieldWrapper>
              <InputFieldWrapper>
                <Input
                  placeholder={'Email'}
                  prefixicon={<Email />}
                  value={email}
                  onChange={e => this.onChangeInput('email', e)}
                />
              </InputFieldWrapper>
              <InputFieldWrapper>
                <Input
                  placeholder={'Password'}
                  prefixicon={<Lock />}
                  type="password"
                  value={password}
                  onChange={e => this.onChangeInput('password', e)}
                />
              </InputFieldWrapper>

              <PropertiesField>
                <Button type="submit">
                  {isRegister ? 'Register' : 'Sign in'}
                </Button>
                <Info>
                  {!isRegister ? (
                    <Register onClick={() => this.handleRegisterState(true)}>
                      Register
                    </Register>
                  ) : (
                    <Register onClick={() => this.handleRegisterState(false)}>
                      SignIn
                    </Register>
                  )}
                  <ForgotPassword onClick={() => this.handleModal(true)}>
                    Forgot Password
                  </ForgotPassword>
                  {CustomModal}
                </Info>
              </PropertiesField>
            </FieldWrapper>
          </form>
        </BoxWrapper>
      </FullLayout>
    )
  }
}

export default withRouter(Portal)
