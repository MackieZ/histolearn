import React from 'react'
import Content from '../components/Content'
import styled from 'styled-components'
import {
  Title,
  TableWrapper,
  TR as ReportTR,
  TH as ReportTH,
  TD as ReportTD
} from './report'
import Mark from '../assets/mark.svg'
import File from '../assets/file.svg'
import { Config } from '../config'

const TH = ReportTH.extend`
  border-right: 0 !important;
  text-align: ${props => props.textAlign};
`

const TD = ReportTD.extend`
  border: 0 !important;
  text-align: ${props => props.textAlign};
`

const TR = ReportTR.extend`
  &:nth-child(2n + 3) {
    background: #fffbfb;
  }

  border: 0 !important;
`

const Icon = styled.img`
  width: ${props => props.size || '52'} px;
  height: ${props => props.size || '52'} px;
  cursor: pointer;
`

class Document extends React.Component {
  render() {
    const defaultDocument = Config.defaultLesson.map(lesson => (
      <TR key={`lesson-${lesson.lesson_no}`}>
        <TD width={'10%'}>{lesson.lesson_no}</TD>
        <TD width={'30%'}>{lesson.name}</TD>
        <TD width={'40%'}>{lesson.detail}</TD>
        <TD width={'10%'} textAlign={'center'}>
          <Icon
            src={File}
            size={25}
            onClick={() => {
              window.open(lesson.content, '')
            }}
          />
        </TD>
        <TD width={'10%'} textAlign={'center'}>
          <Icon
            src={Mark}
            size={25}
            onClick={() => {
              window.open(lesson.mark, '')
            }}
          />
        </TD>
      </TR>
    ))
    return (
      <Content>
        <Title>Documents</Title>
        <TableWrapper textAlign={'left'}>
          <TR>
            <TH>No.</TH>
            <TH>Lesson</TH>
            <TH>Details</TH>
            <TH textAlign={'center'}>Download Resources</TH>
            <TH textAlign={'center'}>Mark</TH>
          </TR>

          {defaultDocument}
        </TableWrapper>
      </Content>
    )
  }
}

export default Document
