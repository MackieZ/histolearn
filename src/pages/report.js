import React from 'react'
import styled from 'styled-components'
import Content from '../components/Content'
import { FONTS, COLORS } from '../styles'
import Correct from '../assets/correct.svg'
import Wrong from '../assets/wrong.svg'
import firebase from 'firebase'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { Bar } from 'react-chartjs'

const DisplayWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const Title = styled.div`
  font-weight: 500;
  font-size: ${FONTS.SIZE.BIG}px;
`

const InfoWrapper = styled.div`
  display: block;
`

const Info = styled.div`
  display: inline-block;
  vertical-align: top;

  &:not(:last-child) {
    margin-right: 12px;
  }
`

const InfoName = styled.span`
  color: ${COLORS.RED};
`

export const TableWrapper = styled.table`
  border-collapse: collapse;
  text-align: ${props => props.textAlign || 'center'};
  margin-top: 1rem;
  width: 100%;
`

export const TR = styled.tr`
  &:not(:last-child) {
    border-bottom: 1px solid ${COLORS.GRAY};
  }
`

export const TD = styled.td`
  padding: 1rem;
  width: ${props => props.width};

  &:not(:last-child) {
    border-right: 1px solid ${COLORS.GRAY};
  }
`

export const TH = styled.th`
  padding: 1rem;
  border-bottom: 2px solid ${COLORS.RED};
  &:not(:last-child) {
    border-right: 1px solid ${COLORS.GRAY};
  }
`

const GraphWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 2rem;
`

class Report extends React.Component {
  state = {
    questions: [],
    students: {}
  }
  componentDidMount() {
    const { roomCode } = this.props

    firebase
      .database()
      .ref('rooms/' + roomCode)
      .on('value', snap => {
        const { questions = [], students = {} } = snap.val() || {}
        this.setState({ questions, students })
      })
  }

  render() {
    const { questions = [], students = {} } = this.state

    const stats = Object.values(students).reduce(
      (result, { score }) => {
        if (result.min > score) {
          result.min = score
        }

        if (result.max < score) {
          result.max = score
        }

        result.sum = result.sum + score
        return result
      },
      {
        min: Infinity,
        max: -Infinity,
        sum: 0
      }
    )

    const runningNumber = questions.map((question, index) => (
      <TH key={`question-${index + 1}`} title={question.questionName}>
        {index + 1}
      </TH>
    ))

    const studentsAnswer = Object.values(students).map((student, index) => {
      const questionAnswer = questions.map(question => (
        <TD key="TD">
          {!!question.replies &&
          !!question.replies[student.name] &&
          question.replies[student.name] === 'answer' ? (
            <img
              alt=""
              src={Correct}
              size={15}
              title={`${question[
                question.replies && question.replies[student.name]
              ] || 'No answer'}`}
            />
          ) : (
            <img
              alt=""
              src={Wrong}
              size={15}
              title={`${question[
                question.replies && question.replies[student.name]
              ] || 'No answer'}`}
            />
          )}
        </TD>
      ))

      return (
        <TR key={`student-${index + 1}`}>
          <TD width={'5%'}>{index + 1}</TD>
          <TD width={'15%'}>{student.name}</TD>
          {questionAnswer}
          <TD width={'15%'}>{student.score}</TD>
        </TR>
      )
    })

    const mean = stats.sum / Object.keys(students).length

    const SD = Object.values(students).reduce(
      (result, { score }) => {
        const v1 = Math.pow(score - mean, 2)

        result.sum = result.sum + v1
        return result
      },

      {
        sum: 0
      }
    )

    const data = {
      // labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      labels: Object.values(students).map(student => student.name) || [],
      datasets: [
        // {
        //   label: 'My First dataset',
        //   fillColor: 'rgba(220,220,220,0.5)',
        //   strokeColor: 'rgba(220,220,220,0.8)',
        //   highlightFill: 'rgba(220,220,220,0.75)',
        //   highlightStroke: 'rgba(220,220,220,1)',
        //   data: [65, 59, 80, 81, 56, 55, 40]
        // },
        {
          label: 'Score',
          fillColor: COLORS.GREEN,
          strokeColor: COLORS.GREEN,
          data: Object.values(students).map(student => student.score) || []
        }
      ]
    }

    return (
      <Content>
        <DisplayWrapper>
          <Title>Report</Title>
          <InfoWrapper>
            <Info>
              <InfoName>Max: </InfoName>
              {stats.max}
            </Info>
            <Info>
              <InfoName>Min: </InfoName>
              {stats.min}
            </Info>
            <Info>
              <InfoName>Mean: </InfoName>
              {mean}
            </Info>
            <Info>
              <InfoName>SD: </InfoName>
              {Math.sqrt(SD.sum)}
            </Info>
          </InfoWrapper>
        </DisplayWrapper>
        <TableWrapper>
          <TR>
            <TH>NO.</TH>
            <TH>Name</TH>
            {runningNumber}
            <TH>Score</TH>
          </TR>
          {studentsAnswer}
        </TableWrapper>
        <GraphWrapper>
          <Bar data={data} width={'1000'} height={'400px'} />
        </GraphWrapper>
      </Content>
    )
  }
}

const mapStateToProps = ({
  roomName,
  roomDescription,
  roomTime,
  roomCode
}) => ({
  roomName,
  roomDescription,
  roomTime,
  roomCode
})

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Report)
)
