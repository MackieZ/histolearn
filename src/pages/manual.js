import React from 'react'
import Content from '../components/Content'
import styled from 'styled-components'
import { Title } from './report'
import { FONTS } from '../styles'
import Register from '../assets/Register.svg'
import SignIn from '../assets/SignIn.svg'
import ForgotPassword from '../assets/ForgotPassword.svg'
import Classroom from '../assets/Classroom.svg'
import ClassroomModal from '../assets/ClassroomModal.svg'
import ClassroomModalNew from '../assets/ClassroomModalNew.svg'
import QuestionChoice from '../assets/QuestionChoice.svg'
import QuestionWriting from '../assets/QuestionWriting.svg'
import Qrcode from '../assets/Qrcode.svg'
import YouTube from 'react-youtube'

const ManualWrapper = styled.div`
  &:not(:last-child) {
    margin-bottom: 3rem;
  }

  margin: auto;
  max-width: 600px;
`

const ManualTitle = styled.div`
  font-weight: 600;
  font-size: ${FONTS.SIZE.BIG}px;
  margin-bottom: 0.5rem;
`

const ManualDescription = styled.div`
  margin-top: 0.5rem;
  line-height: 1.5;
`

const ManualImage = styled.img`
  // OG 0.58

  width: 600px;
  height: 348px;
`

const YoutubeWrapper = styled.div`
  margin-bottom: 2rem;
`

class Manual extends React.Component {
  render() {
    const opts = {
      height: '348',
      width: '600',
      playerVars: {
        autoplay: 1
      }
    }

    return (
      <Content>
        <Title>Manual</Title>
        <ManualWrapper>
          <YoutubeWrapper>
            <YouTube
              videoId={'dvClnmsU8pE'}
              opts={opts}
              // onReady={e => e.target.pauseVideo()}
            />
          </YoutubeWrapper>
          <ManualTitle>How to Register</ManualTitle>
          <ManualImage src={Register} />
          <ManualDescription>
            1. Enter your email and password <br />
            2. Click on the “Register” button <br />
            O1. If you have your own account you can click on the “Signin” link{' '}
            <br />
            O2. If you forgot your password you can click on the “Forgot
            Password” link <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>How to SignIn</ManualTitle>
          <ManualImage src={SignIn} />
          <ManualDescription>
            1. Enter your email and password <br />
            2. Click on the “Sign In” button <br />
            O1. If you don’t have your own account you can click on the
            “Register” link
            <br />
            O2. If you forgot your password you can click on the “Forgot
            Password” link
            <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>Forgot password ?</ManualTitle>
          <ManualImage src={ForgotPassword} />
          <ManualDescription>
            1. Enter your email for got the link create new password <br />
            2. Click on the “Submit” button <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>How to create multiple choice classroom</ManualTitle>
          <ManualImage src={Classroom} />
          <ManualDescription>
            1. Click on the "Multiple Choice" tab <br />
            2. Click on the "+ Add New Classroom" button to create new room{' '}
            <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>
            Create multiple choice classroom with default question
          </ManualTitle>
          <ManualImage src={ClassroomModal} />
          <ManualDescription>
            1. Click on te "Default" to create the default question from system{' '}
            <br />
            2. Select the lesson which you want to create the room
            <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>
            Create multiple choice classroom with your question
          </ManualTitle>
          <ManualImage src={ClassroomModalNew} />
          <ManualDescription>
            1. Click on the "New" button to create with new question <br />
            2. Fill room’s name in this field <br />
            3. Fill room’s detail in this field <br />
            4. Click on the “Submit” button to create the room <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualImage src={QuestionChoice} />
          <ManualDescription>
            5. Create question in this field <br />
            6. Enter the answer in this field
            <br />
            7. Create choice 1 in this field (not answer)
            <br />
            8. Create choice 2 in this field (not answer)
            <br />
            9. Create choice 3 in this field (not answer)
            <br />
            10. Set the time of this quiz
            <br />
            S1. Save to edit again later
            <br />
            S2. Click on the “Confirm” button to confirm classroom (can not
            edit)
            <br />
            S3. Click on the “More Question” button to add more question
            <br />
            S4. Back to previous question
            <br />
            S5. Forward to next question
            <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>How to create writing classroom</ManualTitle>
          <ManualImage src={QuestionWriting} />
          <ManualDescription>
            1. Create question in this field
            <br />
            2. Set the time of this quiz
            <br />
            S1. Save to edit again later
            <br />
            S2. Click on the “Confirm” button to confirm classroom (can not
            edit)
            <br />
            S3. Click on the “More Question” button to add more question
            <br />
            S4. Back to previous question
            <br />
            S5. Forward to next question
            <br />
          </ManualDescription>
        </ManualWrapper>
        <ManualWrapper>
          <ManualTitle>How to start the quiz</ManualTitle>
          <ManualImage src={Qrcode} />
          <ManualDescription>
            1. Click on the "Start" button to start the quiz
            <br />
          </ManualDescription>
        </ManualWrapper>
      </Content>
    )
  }
}

export default Manual
