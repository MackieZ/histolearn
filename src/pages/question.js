import React from 'react'
import Content from '../components/Content'
import styled from 'styled-components'
import RoomDetails from '../components/RoomDetails'
import { WrapperCol } from './rank'
import { Input } from 'antd'
import { SHADOWS, COLORS } from '../styles'
import Button from '../components/Button'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { setField } from '../reducer'
import firebase from 'firebase'
import { pathRoute } from '../App'
import Config from '../config'
import Close from '../assets/close.svg'

const styles = {
  ButtonOutline: {
    background: 'transparent',
    border: `.75px solid ${COLORS.RED}`,
    color: `${COLORS.RED}`
  }
}

const TimerWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`

const TimerLabel = styled.span`
  margin: 0 1rem;

  &:last-child {
    margin-right: 0;
  }
`

const QuestionWrapper = styled.div`
  padding: 2.5rem;
  padding-bottom: 0;
  position: relative;
`

const QuestionBox = styled.div`
  padding: 2.5rem;
  position: relative;
  margin: auto;
  max-width: 600px;
  box-shadow: ${SHADOWS.CARD};
`

const QuestionField = styled.div`
  margin: 0.5rem 0;
  line-height: 2.3125;
`

const QuestionLabel = styled.div`
  color: ${COLORS.GRAY};
`

const Index = styled.div`
  color: ${COLORS.GRAY};
  margin: 1rem 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const ButtonGroupWrapper = styled.div`
  // max-width: 460px
  margin-left: auto;
  margin-right: auto;
  margin-top: 1.5rem;
  display: flex;
  justify-content: space-around;
  align-items: center;
`

const NextIconWrapper = styled.div`
  position: absolute;
  top: 45%;
  right: -1rem;
  cursor: pointer;
`

const PreviousIconWrapper = styled.div`
  position: absolute;
  top: 45%;
  left: -1rem;
  cursor: pointer;
  transform: rotate(180deg);
`

const VectorSVG = props => (
  <svg
    width="22"
    height="37"
    viewBox="0 0 22 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0.714225 0.686817C0.237942 1.14419 -1.57267e-06 1.68635 -1.54477e-06 2.3128L-1.02994e-07 34.6873C-7.50837e-08 35.314 0.237944 35.8558 0.714227 36.3136C1.19104 36.7713 1.75475 37 2.40643 37C3.05798 37 3.6217 36.7713 4.09811 36.3136L20.9408 20.1262C21.4171 19.668 21.6555 19.1262 21.6555 18.5C21.6555 17.8738 21.4171 17.3315 20.9408 16.8741L4.09798 0.686817C3.6217 0.229572 3.05798 3.6835e-06 2.4063 3.71146e-06C1.75475 3.73941e-06 1.19103 0.229572 0.714225 0.686817Z"
      fill={props.fill || '#FFBA6C'}
    />
  </svg>
)

const CloseButton = styled.img`
  width: 20px;
  height: 20px;

  position: absolute;
  top: 1rem;
  right: 1rem;
  cursor: pointer;
`

class Question extends React.Component {
  state = {
    currentIndex: 1,
    boxLength: 1,
    questions: []
  }

  componentDidMount() {
    const { roomCode } = this.props
    if (roomCode) {
      firebase
        .database()
        .ref('rooms/' + roomCode)
        .once('value')
        .then(snapshot => {
          if (snapshot) {
            const { questions } = snapshot.val()
            this.setState({
              questions,
              boxLength: questions.length
            })
          }
        })
    } else {
      firebase
        .database()
        .ref('rooms')
        .once('value')
        .then(snapshot => {
          let en = true
          while (en) {
            const str = Math.random()
              .toString(36)
              .replace(/[^a-zA-Z0-9]+/g, '')
              .substr(1, 6)

            if (!snapshot.val()) {
              const { setField, defaultLesson, roomMode } = this.props
              setField('roomCode', str)
              if (defaultLesson) {
                if (roomMode === 'choice') {
                  this.setState({
                    questions:
                      Config.defaultLessonQuestionChoice[defaultLesson],
                    boxLength:
                      Config.defaultLessonQuestionChoice[defaultLesson].length
                  })
                } else {
                  this.setState({
                    questions:
                      Config.defaultLessonQuestionReplies[defaultLesson],
                    boxLength:
                      Config.defaultLessonQuestionChoice[defaultLesson].length
                  })
                }
              } else {
                this.addField()
              }
              en = false
            }

            if (snapshot.val() && !snapshot.val()[str]) {
              const { setField, defaultLesson, roomMode } = this.props
              setField('roomCode', str)
              if (defaultLesson) {
                if (roomMode === 'choice') {
                  this.setState({
                    questions:
                      Config.defaultLessonQuestionChoice[defaultLesson],
                    boxLength:
                      Config.defaultLessonQuestionChoice[defaultLesson].length
                  })
                } else {
                  this.setState({
                    questions:
                      Config.defaultLessonQuestionReplies[defaultLesson],
                    boxLength:
                      Config.defaultLessonQuestionChoice[defaultLesson].length
                  })
                }
              } else {
                this.addField()
              }

              en = false
            }
          }
        })
    }
  }

  removeField = ind => {
    const { currentIndex, boxLength, questions } = this.state
    if (boxLength > 1) {
      const q = questions.filter((question, index) => index !== ind)
      this.setState({
        questions: q,
        boxLength: q.length,
        currentIndex: currentIndex - (currentIndex > q.length ? 1 : 0)
      })
    }
  }

  confirmQuestion = () => {
    const { openSnackbarMessage } = this.props
    const {
      roomName,
      roomDescription,
      roomMode,
      roomCode,
      roomTime
    } = this.props

    let ret = false
    this.state.questions.forEach(question => {
      Object.keys(question).map(q => {
        if (!question[q]) {
          ret = true
        }
      })
    })
    if (ret) {
      return openSnackbarMessage('Please fill all blank field')
    }

    firebase
      .database()
      .ref('rooms/' + roomCode)
      .set({
        questions: this.state.questions,
        name: roomName,
        description: roomDescription,
        code: roomCode,
        mode: roomMode,
        time: roomTime,
        status: 'waiting',
        owner: firebase.auth().currentUser.uid
      })
      .then(() => {
        this.props.history.push(pathRoute.Qrcode.path)
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  saveQuestion = () => {
    const { openSnackbarMessage } = this.props
    const {
      roomName,
      roomDescription,
      roomMode,
      roomCode,
      roomTime
    } = this.props

    let ret = false
    this.state.questions.forEach(question => {
      Object.keys(question).map(q => {
        if (!question[q]) {
          ret = true
        }
      })
    })
    if (ret) {
      return openSnackbarMessage('Please fill all blank field')
    }

    firebase
      .database()
      .ref('rooms/' + roomCode)
      .set({
        questions: this.state.questions,
        name: roomName,
        description: roomDescription,
        code: roomCode,
        mode: roomMode,
        time: roomTime,
        status: 'save',
        owner: firebase.auth().currentUser.uid
      })
      .then(() => {
        openSnackbarMessage('Save question success')
      })
      .catch(({ message }) => {
        openSnackbarMessage(message)
      })
  }

  addQuestion = () => {
    const { boxLength } = this.state
    this.setState(
      { boxLength: boxLength + 1, currentIndex: boxLength + 1 },
      () => this.addField()
    )
  }

  onClickPrevious = () => {
    const { currentIndex } = this.state
    const canClick = currentIndex > 1 ? 1 : 0
    this.setState({ currentIndex: currentIndex - canClick })
  }

  onClickNext = () => {
    const { currentIndex, boxLength } = this.state
    const canClick = currentIndex < boxLength ? 1 : 0
    this.setState({ currentIndex: currentIndex + canClick })
  }

  handlerQuestionInput = (e, ind, fieldName) => {
    const q = this.state.questions
    q[ind][fieldName] = e.target.value
    this.setState({
      questions: q
    })
  }

  addField = () => {
    const q = this.state.questions
    this.props.roomMode === 'choice'
      ? this.setState({
          questions: [
            ...q,
            {
              questionName: '',
              answer: '',
              choice1: '',
              choice2: '',
              choice3: ''
            }
          ]
        })
      : this.setState({
          questions: [
            ...q,
            {
              questionName: ''
            }
          ]
        })
  }

  render() {
    const {
      roomName = 'Histolearn',
      roomDescription = 'New way to learn history',
      roomTime = 10,
      roomMode = 'choice',
      setField
    } = this.props

    const { currentIndex, boxLength, questions } = this.state

    return (
      <Content>
        <WrapperCol width={'20%'} />
        <WrapperCol width={'60%'}>
          <RoomDetails name={roomName} description={roomDescription} />
          <QuestionWrapper>
            <QuestionBox>
              {roomMode === 'choice' ? (
                <React.Fragment>
                  <QuestionField>
                    <QuestionLabel>Question</QuestionLabel>
                    <Input
                      type="text"
                      value={
                        questions[currentIndex - 1] &&
                        questions[currentIndex - 1].questionName
                      }
                      onChange={e =>
                        this.handlerQuestionInput(
                          e,
                          currentIndex - 1,
                          'questionName'
                        )
                      }
                    />
                  </QuestionField>
                  <QuestionField>
                    <QuestionLabel>Choice 1 - correct answer</QuestionLabel>
                    <Input
                      type="text"
                      value={
                        questions[currentIndex - 1] &&
                        questions[currentIndex - 1].answer
                      }
                      onChange={e =>
                        this.handlerQuestionInput(e, currentIndex - 1, 'answer')
                      }
                    />
                  </QuestionField>
                  <QuestionField>
                    <QuestionLabel>Choice 2 - incorrect</QuestionLabel>
                    <Input
                      type="text"
                      value={
                        questions[currentIndex - 1] &&
                        questions[currentIndex - 1].choice1
                      }
                      onChange={e =>
                        this.handlerQuestionInput(
                          e,
                          currentIndex - 1,
                          'choice1'
                        )
                      }
                    />
                  </QuestionField>
                  <QuestionField>
                    <QuestionLabel>Choice 2 - incorrect</QuestionLabel>
                    <Input
                      type="text"
                      value={
                        questions[currentIndex - 1] &&
                        questions[currentIndex - 1].choice2
                      }
                      onChange={e =>
                        this.handlerQuestionInput(
                          e,
                          currentIndex - 1,
                          'choice2'
                        )
                      }
                    />
                  </QuestionField>
                  <QuestionField>
                    <QuestionLabel>Choice 3 - incorrect</QuestionLabel>
                    <Input
                      type="text"
                      value={
                        questions[currentIndex - 1] &&
                        questions[currentIndex - 1].choice3
                      }
                      onChange={e =>
                        this.handlerQuestionInput(
                          e,
                          currentIndex - 1,
                          'choice3'
                        )
                      }
                    />
                  </QuestionField>
                </React.Fragment>
              ) : (
                <QuestionField>
                  <QuestionLabel>Question</QuestionLabel>
                  <Input.TextArea
                    rows={15}
                    value={
                      questions[currentIndex - 1] &&
                      questions[currentIndex - 1].questionName
                    }
                    onChange={e =>
                      this.handlerQuestionInput(
                        e,
                        currentIndex - 1,
                        'questionName'
                      )
                    }
                  />
                </QuestionField>
              )}
            </QuestionBox>
            <Index>
              {currentIndex}/{boxLength}
            </Index>

            <NextIconWrapper onClick={this.onClickNext}>
              <VectorSVG
                fill={`${
                  currentIndex < boxLength ? COLORS.YELLOW : COLORS.GRAY
                }`}
              />
            </NextIconWrapper>
            <PreviousIconWrapper onClick={this.onClickPrevious}>
              <VectorSVG
                fill={`${currentIndex > 1 ? COLORS.YELLOW : COLORS.GRAY}`}
              />
            </PreviousIconWrapper>
          </QuestionWrapper>
          <ButtonGroupWrapper>
            <Button {...styles.ButtonOutline} onClick={this.saveQuestion}>
              Save
            </Button>
            <Button {...styles.ButtonOutline} onClick={this.confirmQuestion}>
              Finalize Quiz
            </Button>
            <Button onClick={this.addQuestion}>Add Question</Button>
            <Button
              src={Close}
              onClick={() => this.removeField(currentIndex - 1)}
            >
              Delete Question
            </Button>
          </ButtonGroupWrapper>
        </WrapperCol>
        <WrapperCol width={'20%'}>
          <TimerWrapper>
            <TimerLabel>Time:</TimerLabel>
            <Input
              min="1"
              type="number"
              value={roomTime}
              onChange={e => setField('roomTime', e.target.value)}
            />{' '}
            <TimerLabel>Minutes</TimerLabel>
          </TimerWrapper>
        </WrapperCol>
      </Content>
    )
  }
}

const mapStateToProps = ({
  defaultLesson,
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
}) => ({
  defaultLesson,
  roomName,
  roomDescription,
  roomTime,
  roomCode,
  roomMode
})

const mapStateToDispatch = { setField }

export default withRouter(
  connect(
    mapStateToProps,
    mapStateToDispatch
  )(Question)
)
