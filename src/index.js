import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import 'antd/dist/antd.css'
import firebase from 'firebase'
import { Provider } from 'react-redux'
import Reducer from './reducer'
import { createStore } from 'redux'

const store = createStore(
  Reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyBnfGWmyxb-0n0Cv348N7gbelqkwhMau1A',
  authDomain: 'histolearn-unesco.firebaseapp.com',
  databaseURL: 'https://histolearn-unesco.firebaseio.com',
  projectId: 'histolearn-unesco',
  storageBucket: 'histolearn-unesco.appspot.com',
  messagingSenderId: '742117947556'
}

firebase.initializeApp(config)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
