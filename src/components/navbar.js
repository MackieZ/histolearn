import React from 'react'
import styled from 'styled-components'
import { SHADOWS, COLORS } from '../styles'
import Logo from '../assets/logo.png'
import { LAYOUT_MAX_WIDTH, LAYOUT_PADDINGX } from '../styles'
import { pathRoute } from '../App'
import { withRouter } from 'react-router-dom'
import firebase from 'firebase'
import { setEmail } from '../reducer'
import { connect } from 'react-redux'

export const styles = {
  menuGap: 36,
  infoGap: 18
}

const LeftSide = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const MenuLeft = styled.div`
  cursor: pointer;
  color: ${props => props.active && COLORS.RED};

  &:hover {
    color: ${COLORS.RED};
  }

  &:not(:last-child) {
    margin-right: ${styles.menuGap}px;
  }
`

const RightSide = styled.div`
  display: flex;
  justify-content: end;
  align-items: center;
`

const LogoWrapper = styled.img`
  width: 162px;
  height: 44px;
  margin-right: ${styles.menuGap}px;
  margin-bottom: 7px;
`

const Wrapper = styled.div`
  background: ${COLORS.WHITE};
  height: 67px;
  box-shadow: ${SHADOWS.NAV_BAR};
`

const Content = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  padding: 0 ${LAYOUT_PADDINGX}px;
  max-width: ${LAYOUT_MAX_WIDTH}px;
  margin: auto;
`

const UserInfo = styled.div`
  border-right: 1px solid ${COLORS.GRAY};
  color: ${COLORS.GRAY};
  padding-right: ${styles.infoGap}px;
`

const MenuRight = styled.div`
  padding-left: ${styles.infoGap}px;
  cursor: pointer;
`

class Navbar extends React.Component {
  componentDidMount() {
    const { setEmail } = this.props

    firebase.auth().onAuthStateChanged(user => {
      !user && this.props.history.push('/')

      setEmail(user.email)
    })
  }

  onClickLogout = () => {
    firebase.auth().signOut()
  }

  render() {
    const username = this.props.email || 'Histolearn'

    return (
      <Wrapper>
        <Content>
          <LeftSide>
            <LogoWrapper
              src={Logo}
              onCLick={() => this.props.history.push('/')}
            />
            <MenuLeft
              active={this.props.location.pathname === pathRoute.Classroom.path}
              onClick={() => this.props.history.push(pathRoute.Classroom.path)}
            >
              Classrooms
            </MenuLeft>
            <MenuLeft
              active={this.props.location.pathname === pathRoute.Document.path}
              onClick={() => this.props.history.push(pathRoute.Document.path)}
            >
              Resources
            </MenuLeft>
            <MenuLeft
              active={this.props.location.pathname === pathRoute.Manual.path}
              onClick={() => this.props.history.push(pathRoute.Manual.path)}
            >
              Manual
            </MenuLeft>
          </LeftSide>
          <RightSide>
            <UserInfo>
              <MenuRight>{username}</MenuRight>
            </UserInfo>
            <MenuRight onClick={this.onClickLogout}>Logout</MenuRight>
          </RightSide>
        </Content>
      </Wrapper>
    )
  }
}

const mapStateToProps = ({ email }) => ({ email })
const mapStateToDispatch = { setEmail }

export default withRouter(
  connect(
    mapStateToProps,
    mapStateToDispatch
  )(Navbar)
)
