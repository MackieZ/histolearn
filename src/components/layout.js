import React from 'react'
import styled from 'styled-components'
import { LAYOUT_MAX_WIDTH, COLORS } from '../styles'

export const MainLayout = styled.div`
  min-width: ${LAYOUT_MAX_WIDTH}px;
  min-height: 100vh;
  margin: auto;
  background: ${COLORS.BACKGROUND};
`

export const Layout = props => <MainLayout>{props.children}</MainLayout>

export default Layout
