import React from 'react'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { COLORS } from '../styles'

const theme = createMuiTheme({
  overrides: {
    // Name of the component ⚛️ / style sheet
    MuiInput: {
      // Name of the rule
      root: {
        // Some CSS
        // color: COLORS.GRAY
        padding: '6px 0 4px !important'
      }
    }
  }
})

export const EnhanceInput = props => (
  <MuiThemeProvider theme={theme}>
    <FormControl style={{ width: '100%' }}>
      <Input
        style={{ padding: '.5rem 0' }}
        fullWidth
        placeholder={props.placeholder}
        value={props.value}
        onChange={props.onChange}
        startAdornment={
          props.prefixicon ? (
            <InputAdornment
              style={{ marginRight: '1.5rem', color: COLORS.GRAY }}
              position="start"
            >
              {props.prefixicon}
            </InputAdornment>
          ) : (
            ''
          )
        }
        error={props.error}
        {...props}
      />
    </FormControl>
  </MuiThemeProvider>
)

export default EnhanceInput
