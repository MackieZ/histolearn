import React from 'react'
import { LAYOUT_PADDINGX, LAYOUT_MAX_WIDTH, LAYOUT_PADDINGY } from '../styles'
import styled from 'styled-components'

const Wrapper = styled.div`
  height: 100%;
  padding: ${LAYOUT_PADDINGY}px ${LAYOUT_PADDINGX}px;
  max-width: ${LAYOUT_MAX_WIDTH}px;
  margin: auto;
  background-color: ${props => props.backgroundColor};
`

const Content = props => <Wrapper {...props}>{props.children}</Wrapper>

export default Content
