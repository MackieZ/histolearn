import React from 'react'
import Button from '@material-ui/core/Button'
import { COLORS, BORDER, FONTS } from '../styles'

// We can use inline-style
const style = {
  background: `linear-gradient(90deg, ${COLORS.GRADIENT_START}, ${
    COLORS.GRADIENT_STOP
  })`,
  borderRadius: BORDER.BUTTON_RADIUS,
  // border: `.75px solid ${COLORS.RED}`,
  color: COLORS.WHITE,
  padding: '0 2rem',
  textTransform: 'none',
  fontSize: FONTS.SIZE.DEFAULT
}

export const ButtonOverride = props => {
  return (
    <Button
      type={props.type}
      className={props.className}
      style={{ ...style, ...props }}
      onClick={props.onClick}
      disabled={props.disabled}
    >
      {props.children}
    </Button>
  )
}

export default ButtonOverride
