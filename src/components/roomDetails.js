import React from 'react'
import styled from 'styled-components'
import { FONTS, COLORS } from '../styles'

const Wrapper = styled.div`
  text-align: center;
  line-height: 1.75;
`

const Code = styled.div`
  font-size: ${FONTS.SIZE.EXTRA}px;
  font-weight: 600;
`

const Name = styled.div`
  color: ${COLORS.RED};
  font-weight: 500;
`

const Description = styled.div`
  color: ${COLORS.GRAY};
`

const Time = styled.div`
  &:before,
  &:after {
    content: '';
    height: 3px;
    background: ${COLORS.BLACK};
    display: inline-block;
    position: relative;
    width: 20%;
    vertical-align: middle;
    margin: 0 15px;
  }
`

const RoomDetails = props => {
  return (
    <Wrapper>
      {!!props.code && <Code>{props.code}</Code>}
      {!!props.name && <Name>Room: {props.name}</Name>}
      {!!props.description && <Description>{props.description}</Description>}
      {!!props.time && <Time>{props.time} Minutes</Time>}
    </Wrapper>
  )
}

export default RoomDetails
