import React from 'react'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import styled from 'styled-components'
import Close from '../assets/close.svg'

const DialogSubTitle = styled.div``

const DialogWrapper = styled.div`
  padding: 1.5rem 0;
  padding-top: 1.315rem;
  text-align: center;
  margin: auto;
  max-width: 480px;
  min-width: 350px;
`

const CloseButton = styled.img`
  width: 20px;
  height: 20px;

  position: absolute;
  top: 1rem;
  right: 1rem;
  cursor: pointer;
`

const DialogCustom = styled.div``

class Modal extends React.Component {
  render() {
    const { onClose, title, subTitle, children = null, ...other } = this.props

    return (
      <Dialog
        onClose={onClose}
        aria-labelledby="dialog"
        {...other}
        maxWidth={'sm'}
        fullWidth
      >
        <DialogWrapper>
          <DialogTitle id="dialog">{title}</DialogTitle>
          <CloseButton src={Close} onClick={onClose} />
          {!!subTitle && <DialogSubTitle>{subTitle}</DialogSubTitle>}
          <DialogCustom>{children}</DialogCustom>
        </DialogWrapper>
      </Dialog>
    )
  }
}

export default Modal
