import React from 'react'
import styled from 'styled-components'
import { COLORS, FONTS } from '../styles'
import Plus from '../assets/plus.svg'
import Close from '../assets/close.svg'
import { Config } from '../config'

const BoxWrapper = styled.div`
  width: 220px;
  height: 220px;
  display: inline-block;
  vertical-align: top;

  margin-right: 3rem;
  margin-bottom: 2rem;
  border-radius: 5px;

  color: ${props => (props.addClassroom ? 'white' : `${COLORS.BLACK}`)};
  background: ${props =>
    props.addClassroom
      ? `linear-gradient(to right, ${COLORS.GRADIENT_START}, ${
          COLORS.GRADIENT_STOP
        })`
      : 'white'};
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
  cursor: pointer;
`

const AddRoomWrapper = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const DetailsWrapper = styled.div`
  text-align: center;
`

const Icon = styled.img`
  width: ${props => props.size || '52'} px;
  height: ${props => props.size || '52'} px;
`

const AddClassTitle = styled.div`
  margin-top: 1rem;
  color: white;
  font-size: ${FONTS.SIZE.BIG};
`

const RoomContent = styled.div`
  height: 100%;
  padding: 0.5rem;
`

const RoomWrapper = styled.div`
  height: 100%;
  position: relative;
`

const CloseButton = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  color: #bcbcbc;
`

const StatusRoom = styled.div`
  position: absolute;
  right: 0;
  bottom: 0;
  font-size: ${FONTS.SIZE.SMALL}px;
  color: ${props => Config.statusRoomColor[props.status]};
  text-transform: capitalize;
`

const RoomDetails = styled.div`
  padding: 1rem;
`

const RoomTitle = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem;
  font-size: ${FONTS.SIZE.EXTRA - 8}px;
`

const RoomName = styled.div`
  margin-top: 1rem;
  line-height: 1.1;
  height: 2.2em;
  overflow: hidden;
  text-overflow: ellipsis;
`

const RoomDescription = styled.div`
  line-height: 1.1;
  color: ${COLORS.GRAY};
  height: 2.2em;
  overflow: hidden;
  text-overflow: ellipsis;
`

const HR = styled.div`
  margin: 0.5rem 0;
  height: 0px;
  border-bottom: 1px dashed ${COLORS.GRAY};
`

export const RoomBox = props => {
  const isAdd = !!props.addClassroom
  const { onDelete = e => e.stopPropagation() } = props

  return (
    <BoxWrapper {...props}>
      {isAdd && (
        <AddRoomWrapper>
          <DetailsWrapper>
            <Icon src={Plus} />
            <AddClassTitle>Add New Classroom</AddClassTitle>
          </DetailsWrapper>
        </AddRoomWrapper>
      )}
      {!isAdd && (
        <RoomContent>
          <RoomWrapper>
            <CloseButton>
              <Icon src={Close} size={25} onClick={onDelete} />
            </CloseButton>
            <RoomDetails>
              <RoomTitle>{props.code}</RoomTitle>
              <RoomName>{props.name}</RoomName>
              <HR />
              <RoomDescription>{props.description}</RoomDescription>
            </RoomDetails>
            <StatusRoom status={props.status}>{props.status}</StatusRoom>
          </RoomWrapper>
        </RoomContent>
      )}
    </BoxWrapper>
  )
}

export default RoomBox
