const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({
  origin: true
})
const moment = require('moment')

admin.initializeApp()

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
  const timestamp = new Date()
  const date = moment().format('X')
  response.send(date)
})

exports.timestamp = functions.https.onRequest((request, response) => {
  const timestamp = moment().format('X')
  return cors(request, response, () => {
    response.status(200).send(timestamp)
  })
})

exports.getTime = functions.https.onRequest((request, response) => {
  const timestamp = new Date()
  return cors(request, response, () => {
    response.status(200).send(timestamp)
  })
})

exports.accessRoom = functions.https.onRequest((request, response) => {
  const timestamp = moment().format('X')
  const { code } = request.query
  return cors(request, response, () => {
    admin
      .database()
      .ref('rooms/' + code)
      .once('value', function(snap) {
        if (snap === null) {
          return response
            .status(404)
            .send({ error: { code: 404, message: 'Room not found' } })
        } else {
          const timeEnd = snap.child('timeEnd').val()

          if (!timeEnd) {
            return response
              .status(404)
              .send({ error: { code: 404, message: 'Time ending not found' } })
          }
          const timeDiff = timeEnd - timestamp
          if (timeDiff <= 0) {
            response.status(200).send('done')
          } else {
            response.status(200).send('' + timeDiff)
          }
        }
      })
  })
})
